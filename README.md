# Spa Vanilla GemaMesasV

![EXAMPLE](example.PNG)

## Installation

Clone the repo:

`git clone https://gitlab.com/GemaMesasV/spa-vanilla-gemamesasv.git `

Install NPM packages

`npm install`

## Usage

To run the app you need to use the following command:

`npm run start`

To run the tests you need to use the following command:

`npm run test`
