import "../components/posts-list";
import "../components/post-detail";

export class HomePage extends HTMLElement {
  constructor() {
    super();
  }

  getStyles() {
    return `
    <style>
      home-page {
        width: 100%;
        height: 100%;
        display: grid;
        grid-template-columns: 1fr 2fr;
        grid-gap: 50px;
      }
    </style>
    `;
  }

  connectedCallback() {
    this.innerHTML = `${this.getStyles()}
    <posts-list class="post-list-section" id="postsList"></posts-list>
    <post-detail class="post-detail-section" id="postDetail"></post-detail>
    `;
    const postDetail = this.querySelector("#postDetail");

    this.addEventListener("post:selected", (e) => {
      postDetail.handleSelectedPost(e.detail);
    });
    this.addEventListener("mode:addMode", () => {
      postDetail.addMode();
    });
  }
}
customElements.define("home-page", HomePage);
