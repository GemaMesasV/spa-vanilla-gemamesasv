import { LitElement, html } from "lit";
import { AllPostsUseCase } from "../usecases/all-posts.usecase";
import { state } from "../states/state";
import { snapshot, subscribe } from "valtio/vanilla";
import "../ui/ui-post-detail";

export class PostsList extends LitElement {
  static get properties() {
    return {
      posts: { type: Array },
    };
  }

  async connectedCallback() {
    super.connectedCallback();

    this.unsubscribe = subscribe(state, () => {
      this.posts = snapshot(state).posts;
      this.render();
    });
    state.posts = await AllPostsUseCase.execute();
  }
  disconnectedCallback() {
    this.unsubscribe();
  }
  render() {
    const isPostsRoute = window.location.pathname === "/posts";

    if (isPostsRoute) {
      return html`
        <h1 class="title">Posts list</h1>
        <ul class="post__list">
          ${this.posts?.map(
            (post) =>
              html`<li
                @click="${() => this.postSelectedClick(post)}"
                class="post__list--item"
              >
                <ui-post-detail .post=${post}></ui-post-detail>
              </li>`
          )}
        </ul>
      `;
    }

    return html`
      <button @click="${this.addModeClick}" class="button">Add Mode</button>
      <h1 class="title">Posts list</h1>
      <ul class="post__list">
        ${this.posts?.map(
          (post) =>
            html`<li
              id="post_${post.id}"
              @click="${() => this.postSelectedClick(post)}"
              class="post__list--item"
            >
              <ui-post-detail .post=${post}></ui-post-detail>
            </li>`
        )}
      </ul>
    `;
  }

  createRenderRoot() {
    return this;
  }
  postSelectedClick(post) {
    const postSelected = new CustomEvent("post:selected", {
      detail: post,
      bubbles: true,
      composed: true,
    });
    this.dispatchEvent(postSelected);
  }
  addModeClick(e) {
    e.preventDefault();
    this.dispatchEvent(
      new CustomEvent("mode:addMode", {
        bubbles: true,
        composed: true,
      })
    );
  }
}

customElements.define("posts-list", PostsList);
