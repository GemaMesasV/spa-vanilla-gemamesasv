import { LitElement, html } from "lit";
import "../ui/ui-post-detail";
import "../ui/ui-button";
import { CreatePostUseCase } from "../usecases/create-post.usecase";
import { UpdatePostUseCase } from "../usecases/update-post.usecase";
import { DeletePostUseCase } from "../usecases/delete-post.usecase";

import { state } from "../states/state";
import { Post } from "../model/post";

export class PostDetail extends LitElement {
  connectedCallback() {
    super.connectedCallback();
    this.titleValue = "";
    this.contentValue = "";
    this.postId = "";
  }

  static get properties() {
    return {
      titleValue: { type: String },
      contentValue: { type: String },
      postId: { type: Number },
    };
  }

  render() {
    return html`
      <section>
        <h2 class="title">Post detail</h2>
        <form class="form" action="">
          <label class="form__label"
            >Title<input
              id="inputTitle"
              class="form__label--input"
              type="text"
              @change="${this.setTitleInput}"
              .value="${this.titleValue}"
              placeholder="Add title here..."
              size="50"
          /></label>
          <label class="form__label"
            >Body<input
              id="inputContent"
              class="form__label--input"
              type="text"
              @change="${this.setContentInput}"
              .value="${this.contentValue}"
              placeholder="Add content here..."
              size="50"
          /></label>
          <div>
            <ui-button
              id="addButton"
              type="reset"
              @click="${this.addClick}"
              ?hidden="${this.postId}"
              buttonContent="Add"
            >
            </ui-button>
            <ui-button
              id="cancelButton"
              type="reset"
              @click="${this.cancelClick}"
              buttonContent="Cancel"
            ></ui-button>
            <ui-button
              id="updateButton"
              type="submit"
              @click="${this.updateClick}"
              ?hidden="${!this.postId}"
              buttonContent="Update"
            >
            </ui-button>
            <ui-button
              id="deleteButton"
              type="submit"
              @click="${this.deleteClick}"
              ?hidden="${!this.postId}"
              buttonContent="Delete"
            >
            </ui-button>
          </div>
        </form>
      </section>
    `;
  }

  setTitleInput(e) {
    this.titleValue = e.target.value;
  }
  setContentInput(e) {
    this.contentValue = e.target.value;
  }

  resetForm() {
    this.titleValue = "";
    this.contentValue = "";
    this.postId = "";
  }

  addMode() {
    this.resetForm();
  }

  cancelClick(e) {
    e.preventDefault();
    this.resetForm();
  }

  async addClick(e) {
    e.preventDefault();
    const newPost = new Post({
      title: this.titleValue,
      content: this.contentValue,
    });
    const updatedPosts = await CreatePostUseCase.execute(state.posts, newPost);
    state.posts = updatedPosts;
    this.resetForm();
  }

  async deleteClick(e) {
    e.preventDefault();
    const deletedPost = new Post({
      id: this.postId,
      title: this.titleValue,
      content: this.contentValue,
    });
    const updatedPosts = await DeletePostUseCase.execute(
      state.posts,
      deletedPost
    );
    state.posts = updatedPosts;
    this.resetForm();
  }

  async updateClick(e) {
    e.preventDefault();
    const updatedPost = new Post({
      id: this.postId,
      title: this.titleValue,
      content: this.contentValue,
    });
    const updatedPosts = await UpdatePostUseCase.execute(
      state.posts,
      updatedPost
    );
    state.posts = updatedPosts;
    this.resetForm();
  }

  createRenderRoot() {
    return this;
  }
  handleSelectedPost(post) {
    this.titleValue = post.title;
    this.contentValue = post.content;
    this.postId = post.id;
  }
}

customElements.define("post-detail", PostDetail);
