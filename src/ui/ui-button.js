import { LitElement, html } from "lit";

export class UiButton extends LitElement {
  static get properties() {
    return {
      buttonContent: { type: String },
    };
  }

  render() {
    return html` <button class="button">${this.buttonContent}</button> `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("ui-button", UiButton);
