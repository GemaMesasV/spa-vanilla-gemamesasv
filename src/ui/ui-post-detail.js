import { LitElement, html } from "lit";
import "../components/posts-list";

export class UiPostDetail extends LitElement {
  static get properties() {
    return {
      post: { type: Object },
    };
  }

  render() {
    return html`<p class="ui-post-title">${this.post?.title}</p>
      <p class="ui-post-subtitle">${this.post?.content}</p> `;
  }

  createRenderRoot() {
    return this;
  }
}
customElements.define("ui-post-detail", UiPostDetail);
