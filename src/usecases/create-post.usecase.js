import { PostsRepository } from "../repositories/posts.repository";

export class CreatePostUseCase {
  static async execute(posts = [], postModel) {
    const repository = new PostsRepository();
    const postCreated = await repository.createPost(postModel);
    const postModelCreated = {
      id: postCreated.id,
      title: postCreated.title,
      content: postCreated.body,
    };

    return [postModelCreated, ...posts];
  }
}
