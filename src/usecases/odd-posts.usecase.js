import { Post } from "../model/post";
import { PostsRepository } from "../repositories/posts.repository";

export class OddPostsUseCase {
  static async execute() {
    const repository = new PostsRepository();
    const posts = await repository.getAllPosts();
    return posts
      .filter((post) => post.id % 2 !== 0)
      .map(
        (post) =>
          new Post({
            id: post.id,
            title: post.title,
            content: post.body,
          })
      );
  }
}
