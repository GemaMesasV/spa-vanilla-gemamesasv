import { PostsRepository } from "../repositories/posts.repository";

export class DeletePostUseCase {
  static async execute(posts = [], postModel) {
    const repository = new PostsRepository();
    const deleteStatus = await repository.deletePost(postModel);
    return deleteStatus === 200
      ? posts.filter((post) => post.id != postModel.id)
      : posts;
  }
}
