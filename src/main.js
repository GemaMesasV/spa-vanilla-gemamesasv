import "./styles/main.css";
import "./styles/components/header.css";
import "./styles/components/main.css";
import "./styles/components/post-list-section.css";
import "./styles/components/post-detail-section.css";
import "./styles/core/reset.css";
import "./styles/core/variables.css";

import "./pages/home-page";
import "./pages/posts-page";

import { Router } from "@vaadin/router";

const outlet = document.getElementById("outlet");
const router = new Router(outlet);

router.setRoutes([
  { path: "/", component: "home-page" },
  { path: "/posts", component: "posts-page" },
  { path: "(.*)", redirect: "/" },
]);
