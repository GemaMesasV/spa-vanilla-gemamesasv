import { PostsRepository } from "../src/repositories/posts.repository";
import { OddPostsUseCase } from "../src/usecases/odd-posts.usecase";
import { POSTS } from "./fixtures/posts";

jest.mock("../src/repositories/posts.repository");
describe("Odd posts use case", () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it("should get odd posts", async () => {
    PostsRepository.mockImplementation(() => {
      return {
        getAllPosts: () => {
          return POSTS;
        },
      };
    });

    const posts = await OddPostsUseCase.execute();

    expect(posts.length).toBe(50);

    expect(posts[1].title).toBe(POSTS[2].title);
    expect(posts[posts.length - 1].content).toBe(POSTS[98].body);
  });
});
