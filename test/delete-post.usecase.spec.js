import { Post } from "../src/model/post";
import { PostsRepository } from "../src/repositories/posts.repository";
import { DeletePostUseCase } from "../src/usecases/delete-post.usecase";

jest.mock("../src/repositories/posts.repository");

describe("Delete post use case", () => {
  beforeEach(() => {
    PostsRepository.mockClear();
  });

  it("should executing properly", async () => {
    const post = new Post({
      id: 2,
      content: "Content deleted",
      title: "Title deleted",
    });

    PostsRepository.mockImplementation(() => {
      return {
        deletePost: () => {
          return 200;
        },
      };
    });

    const POSTS = [
      {
        id: 1,
        title:
          "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
        content:
          "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
      },
      {
        id: 2,
        title: "Title deleted",
        content: "Content deleted",
      },
      {
        id: 3,
        title: "qui est esse",
        content:
          "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla",
      },
    ];

    const postsDeleted = await DeletePostUseCase.execute(POSTS, post);

    expect(postsDeleted.length).toBe(2);
    expect(postsDeleted[1].title).toBe(POSTS[2].title);
    expect(postsDeleted[1].content).toBe(POSTS[2].content);
  });
});
