/// <reference types="Cypress" />

describe("spa vanilla gemamesasv spec", () => {
  beforeEach(() => {
    cy.visit("/");
    cy.wait(1000);
  });

  it("user visit the webpage", () => {
    cy.get("#postsPage").click();
    cy.scrollTo("bottom");
    cy.scrollTo("top");
    cy.get("#homePage").click();
    cy.get("#post_1 > ui-post-detail > .ui-post-title").should("exist");
  });

  it("user go to the Add Mode from Update Mode", () => {
    cy.get("#post_1 > ui-post-detail > .ui-post-title").click();
    cy.get("#updateButton > .button").should("be.visible");
    cy.get("#cancelButton > .button").should("be.visible");
    cy.get("#addButton > .button").should("not.be.visible");
    cy.get("#postsList > .button").click();
    cy.get("#updateButton > .button").should("not.be.visible");
    cy.get("#cancelButton > .button").should("be.visible");
    cy.get("#addButton > .button").should("be.visible");
  });

  it("user create a new post", () => {
    cy.get("#inputTitle").type("Test title");
    cy.get("#inputContent").type("Test content");
    cy.get("#addButton > .button").click();
    cy.get("#post_101 > ui-post-detail > .ui-post-title").contains(
      "Test title"
    );
  });
  it("user create a new post but cancel", () => {
    cy.get("#inputTitle").type("Test title");
    cy.get("#inputContent").type("Test content");
    cy.get("#cancelButton > .button").click();
  });

  it("user update a post", () => {
    cy.get("#post_1 > ui-post-detail > .ui-post-title").click();
    cy.get("#inputTitle").type("Test title updated");
    cy.get("#inputContent").type("Test content updated");
    cy.get("#updateButton > .button").click();
    cy.get("#post_1 > ui-post-detail > .ui-post-title").contains(
      "Test title updated"
    );
    cy.get("#updateButton > .button").should("not.be.visible");
    cy.get("#deleteButton > .button").should("not.be.visible");
    cy.get("#cancelButton > .button").should("be.visible");
    cy.get("#addButton > .button").should("be.visible");
  });
  it("user update a post but cancel", () => {
    cy.get("#post_1 > ui-post-detail > .ui-post-title").click();
    cy.get("#inputTitle").type("Test title updated");
    cy.get("#inputContent").type("Test content updated");
    cy.get("#cancelButton > .button").click();
    cy.get("#updateButton > .button").should("not.be.visible");
    cy.get("#deleteButton > .button").should("not.be.visible");
    cy.get("#cancelButton > .button").should("be.visible");
    cy.get("#addButton > .button").should("be.visible");
  });
  it("user delete a post", () => {
    cy.get("#post_1 > ui-post-detail > .ui-post-title").click();
    cy.get("#deleteButton > .button").click();
    cy.get("#post_1 > ui-post-detail > .ui-post-title").should("not.exist");
    cy.get("#updateButton > .button").should("not.be.visible");
    cy.get("#deleteButton > .button").should("not.be.visible");
    cy.get("#cancelButton > .button").should("be.visible");
    cy.get("#addButton > .button").should("be.visible");
  });
  it("user update a post and delete", () => {
    cy.get("#post_1 > ui-post-detail > .ui-post-title").click();
    cy.get("#inputTitle").type("Test title updated");
    cy.get("#inputContent").type("Test content updated");
    cy.get("#updateButton > .button").click();
    cy.get("#updateButton > .button").should("not.be.visible");
    cy.get("#deleteButton > .button").should("not.be.visible");
    cy.get("#cancelButton > .button").should("be.visible");
    cy.get("#addButton > .button").should("be.visible");
    cy.get("#post_1 > ui-post-detail > .ui-post-title").click();
    cy.get("#deleteButton > .button").click();
    cy.get("#post_1 > ui-post-detail > .ui-post-title").should("not.exist");
    cy.get("#updateButton > .button").should("not.be.visible");
    cy.get("#deleteButton > .button").should("not.be.visible");
    cy.get("#cancelButton > .button").should("be.visible");
    cy.get("#addButton > .button").should("be.visible");
  });
});
